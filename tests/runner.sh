#!/bin/sh

case "${1}" in
	("lspst_folders") test "`lspst data.pst | grep -c Folder`" = "5" ;;
	("lspst_mails") test "`lspst data.pst | grep -c Email`" = "3" ;;
	("lspst_contacts") test "`lspst data.pst | grep -c Contact`" = "4" ;;
	("lspst_events") test "`lspst data.pst | grep -c Appointment`" = "1" ;;
	("pst2ldif_run") test "`pst2ldif -b base -c class data.pst | grep -c no.where`" = "3" ;;
	("readpst_basic") test "`readpst data.pst -o /tmp | grep -c Folder`" = "13" ;;
	("readpst_contacts") readpst -tc -cv data.pst >/dev/null && test "`cat Contacts.contacts | grep -c BEGIN:VCARD`" = "3" ;;
	("readpst_contacts_emails") readpst -tc -cv -w data.pst >/dev/null && test "`cat Contacts.contacts | grep -c '@no.where'`" = "3" ;;
	("readpst_contacts_plain") readpst -tc -cl -w data.pst >/dev/null && test "`cat Contacts.contacts | wc -l`" = "3" ;;
	("readpst_contacts_plain_emails") readpst -tc -cl -w >/dev/null data.pst && test "`cat Contacts.contacts | grep -c '@no.where'`" = "3" ;;
	("readpst_events") readpst -ta data.pst >/dev/null && test "`cat Calendar.calendar | grep -c BEGIN:VEVENT`" = "1" ;;
	("readpst_events_summary") readpst -ta -w data.pst >/dev/null && test "`cat Calendar.calendar | grep -c 'SUMMARY:event on 2020-02-13'`" = "1" ;;
	("readpst_mails") readpst -te data.pst >/dev/null && test "`ls -l *.mbox | wc -l`" = "2" ;;
	("readpst_mails_inbox") readpst -te -w data.pst >/dev/null && test "`cat Inbox.mbox | grep -c 'From '`" = "2" ;;
	("readpst_mails_inbox_to") readpst -te -w data.pst >/dev/null && test "`cat Inbox.mbox | grep -c 'To: zyx@no.where'`" = "2" ;;
	("readpst_mails_inbox_cc") readpst -te -w data.pst >/dev/null && test "`cat Inbox.mbox | grep -c -i 'CC'`" = "0" ;;
	("readpst_mails_inbox_email") readpst -te -w data.pst >/dev/null && test "`cat Inbox.mbox | grep -c 'zyx@no.where'`" = "2" ;;
	("readpst_mails_inbox_subject") readpst -te -w data.pst >/dev/null && test "`cat Inbox.mbox | grep -c 'Subject: Plain text message'`" = "2" ;;
	("readpst_mails_inbox_subject_2") readpst -te -w data.pst >/dev/null && test "`cat Inbox.mbox | grep -c 'Subject: Plain text message \]\['`" = "1" ;;
	("readpst_mails_inbox_body") readpst -te -w data.pst >/dev/null && test "`cat Inbox.mbox | grep 'Plain text' | grep -c -v 'Subject:'`" = "1" ;;
	("readpst_mails_inbox_body_2") readpst -te -w data.pst >/dev/null && test "`cat Inbox.mbox | grep -c 'The second plain text'`" = "1" ;;
	("readpst_mails_drafts") readpst -te -w data.pst >/dev/null && test "`cat Drafts.mbox | grep -c 'From '`" = "1" ;;
	("readpst_mails_drafts_to") readpst -te -w data.pst >/dev/null && test "`cat Drafts.mbox | grep -c 'To: zyx@no.where'`" = "1" ;;
	("readpst_mails_drafts_cc") readpst -te -w data.pst >/dev/null && test "`cat Drafts.mbox | grep -c -i 'CC'`" = "0" ;;
	("readpst_mails_drafts_email") readpst -te -w data.pst >/dev/null && test "`cat Drafts.mbox | grep -c 'zyx@no.where'`" = "1" ;;
	("readpst_mails_drafts_subject") readpst -te -w data.pst >/dev/null && test "`cat Drafts.mbox | grep -c 'Subject: Plain text message'`" = "1" ;;
	("readpst_mails_drafts_subject_2") readpst -te -w data.pst >/dev/null && test "`cat Drafts.mbox | grep -c 'Subject: Plain text message \]\['`" = "1" ;;
	("readpst_mails_drafts_body") readpst -te -w data.pst >/dev/null && test "`cat Drafts.mbox | grep 'Plain text' | grep -c -v 'Subject:'`" = "0" ;;
	("readpst_mails_drafts_body_2") readpst -te -w data.pst >/dev/null && test "`cat Drafts.mbox | grep -c 'The second plain text'`" = "1" ;;
	("") echo "runner.sh: Requires one argument, a test name to run" >&2 ; exit 1 ;;
	(*) echo "runner.sh: Unknown test name '%{1}'" >&2 ; exit 1 ;;
esac
